package com.example.timaxapa.neutrinobillboard.domain;

import com.google.gson.annotations.Expose;

import java.util.List;

public class AdvertisementResponse {

    @Expose(deserialize = true)
    private boolean success;
    @Expose(deserialize = true)
    private String message;
    @Expose(deserialize = true)
    private List<Advertisement> ads;

    public AdvertisementResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Advertisement> getAds() {
        return ads;
    }

    public void setAds(List<Advertisement> ads) {
        this.ads = ads;
    }

}
