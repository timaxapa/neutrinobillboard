package com.example.timaxapa.neutrinobillboard.domain;

public class AddAdvertisementResponse {

    private boolean success;
    private String message;

    public AddAdvertisementResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
