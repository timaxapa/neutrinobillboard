package com.example.timaxapa.neutrinobillboard;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.android.volley.*;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.timaxapa.neutrinobillboard.adapters.AdvertisementsListAdapter;
import com.example.timaxapa.neutrinobillboard.domain.Advertisement;
import com.example.timaxapa.neutrinobillboard.domain.AdvertisementResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    private final int AUTH_REQUEST_CODE = 1;
    private final int ADD_REQUEST_CODE = 2;
    private final String REQUESTS_TAG = "REQUESTS_TAG";
    private final String LOG_TAG = "MainActivity";

    private RequestQueue queue;

    private String userId;
    private PreferencesHelper preferencesHelper;

    private ListView lvAdvertisements;
    private EditText etQuery;
    private TextView textView;
    private ImageButton btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queue = Volley.newRequestQueue(this);
        preferencesHelper = new PreferencesHelper(this);

        lvAdvertisements = (ListView) findViewById(R.id.lvAdvertisements);
        textView = (TextView) findViewById(R.id.textView);
        etQuery = (EditText) findViewById(R.id.etQuery);

        btnSearch = (ImageButton) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Logout
        if (item.getItemId() == R.id.logoutAction) {
            preferencesHelper.setUserId(null);
            finish();
        }

        // Add new advertisement
        if (item.getItemId() == R.id.addAdvertisementAction) {
            Intent intent = new Intent(this, AddAdvertisementActivity.class);
            intent.putExtra("UserId", userId);
            startActivityForResult(intent, ADD_REQUEST_CODE);
        }
        return true;
    }

    // Loading list of advertisements
    private void loadAdvertisementsList() {
        // Build query string
        String url = getString(R.string.api_address) +
                "advertisement/search/?query=" + etQuery.getText().toString();

        // Get advertisements request
        StringRequest request = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new GsonBuilder()
                                .setDateFormat("dd.MM.yyyy HH:mm:ss")
                                .excludeFieldsWithoutExposeAnnotation()
                                .create();

                        AdvertisementResponse advertisementResponse;
                        try {
                            advertisementResponse =
                                    gson.fromJson(response, AdvertisementResponse.class);
                        } catch (Exception e) {
                            Log.e(LOG_TAG, e.toString());
                            Toast.makeText(
                                    getApplicationContext(),
                                    getString(R.string.text_error),
                                    Toast.LENGTH_SHORT
                            ).show();
                            return;
                        }

                        if (advertisementResponse.isSuccess()) {
                            // Creating adapter
                            final AdvertisementsListAdapter adapter = new AdvertisementsListAdapter(
                                    getApplicationContext(),
                                    R.layout.advertisements_list_item,
                                    advertisementResponse.getAds()
                            );

                            // Loading images
                            loadImages(advertisementResponse.getAds(), adapter);

                            lvAdvertisements.setAdapter(adapter);
                        } else {
                            Toast.makeText(
                                    getApplicationContext(),
                                    advertisementResponse.getMessage(),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        textView.setText(error.getMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("UserId", userId);
                return headers;
            }
        };

        request.setTag(REQUESTS_TAG);
        queue.add(request);
    }

    // Loading images for every advertisement
    private void loadImages(List<Advertisement> advertisements,
                            final AdvertisementsListAdapter adapter) {

        for (final Advertisement item : advertisements) {
            String url = getString(R.string.api_address)
                    + "advertisement/image/" + item.getId();
            ImageRequest imageRequest = new ImageRequest(
                    url, new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap response) {
                    item.setImage(response);

                    // Refreshing list
                    lvAdvertisements.setAdapter(adapter);
                }
            }, 0, 0, null, null, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.w("Image uploading", error.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("UserId", userId);
                    return params;
                }
            };

            queue.add(imageRequest);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        userId = preferencesHelper.getUserId();
        if (userId == null) {
            Intent intent = new Intent(this, AuthActivity.class);
            startActivityForResult(intent, AUTH_REQUEST_CODE);
            return;
        }
        loadAdvertisementsList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Authorization
        if (requestCode == AUTH_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                userId = data.getStringExtra(PreferencesHelper.USER_ID);
                preferencesHelper.setUserId(userId);
            }
        }

        // Adding new advertisement
        if (requestCode == ADD_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(
                        getApplicationContext(),
                        getString(R.string.text_message_advertisement_added),
                        Toast.LENGTH_SHORT
                ).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSearch) {
            loadAdvertisementsList();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (queue != null) {
            queue.cancelAll(REQUESTS_TAG);
        }
    }


}
