package com.example.timaxapa.neutrinobillboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.timaxapa.neutrinobillboard.domain.AuthResponse;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;


public class AuthActivity extends ActionBarActivity implements View.OnClickListener {

    private final String REQUESTS_TAG = "REQUESTS_TAG";
    private EditText etLogin, etPassword;
    private Button btnLogin, btnRegister;

    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        queue = Volley.newRequestQueue(this);

        etLogin = (EditText) findViewById(R.id.etLogin);
        etPassword = (EditText) findViewById(R.id.etPassword);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        // Building request string
        String url = getString(R.string.api_address) + "user/";
        if (v.getId() == R.id.btnRegister) {
            url += "register/";
        } else if (v.getId() == R.id.btnLogin) {
            url += "login/";
        }

        // Sending request
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        AuthResponse authResponse =
                                gson.fromJson(response, AuthResponse.class);

                        // Checking result
                        if (authResponse.isSuccess()) {
                            Intent intent = new Intent();
                            intent.putExtra(PreferencesHelper.USER_ID, authResponse.getId());
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            Toast.makeText(
                                    getApplicationContext(),
                                    authResponse.getMessage(),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(
                                getApplicationContext(),
                                getString(R.string.text_error),
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("login", etLogin.getText().toString());
                params.put("password", etPassword.getText().toString());
                return params;
            }
        };

        request.setTag(REQUESTS_TAG);
        queue.add(request);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(REQUESTS_TAG);
        }
    }
}
