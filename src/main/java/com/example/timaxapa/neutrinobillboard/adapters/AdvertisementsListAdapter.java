package com.example.timaxapa.neutrinobillboard.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.timaxapa.neutrinobillboard.R;
import com.example.timaxapa.neutrinobillboard.domain.Advertisement;

import java.text.SimpleDateFormat;
import java.util.List;

public class AdvertisementsListAdapter extends ArrayAdapter<Advertisement> {

    private int resource;

    public AdvertisementsListAdapter(Context context, int resource, List<Advertisement> values) {
        super(context, resource, values);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(this.resource, parent, false);

        Advertisement advertisement = getItem(position);

        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        tvName.setText(advertisement.getName());

        TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        tvDescription.setText(advertisement.getDescription());

        SimpleDateFormat format = new SimpleDateFormat("d.MM.yy hh:mm");
        TextView tvCreatedAt = (TextView) view.findViewById(R.id.tvCreatedAt);
        tvCreatedAt.setText(format.format(advertisement.getCreatedAt()).toString());

        ImageView ivImage = (ImageView) view.findViewById(R.id.ivImage);
        if (advertisement.getImage() != null) {
            ivImage.setImageBitmap(advertisement.getImage());
        }

        return view;
    }
}
