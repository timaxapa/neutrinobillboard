package com.example.timaxapa.neutrinobillboard;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.timaxapa.neutrinobillboard.domain.AddAdvertisementResponse;
import com.google.gson.Gson;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class AddAdvertisementActivity extends ActionBarActivity
        implements View.OnClickListener {

    private final int RESULT_LOAD_IMG = 1;
    private final String REQUESTS_TAG = "REQUESTS_TAG";
    private final String LOG_TAG = "AddAdvertisementActivity";

    private Button btnAdd;
    private ImageButton btnLoadImage;
    private EditText etName, etDescription;
    private ImageView ivImage;

    private RequestQueue queue;

    private String imgPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_advertisement);

        etName = (EditText) findViewById(R.id.etName);
        etDescription = (EditText) findViewById(R.id.etDescription);
        ivImage = (ImageView) findViewById(R.id.ivImage);

        queue = Volley.newRequestQueue(this);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
        btnLoadImage = (ImageButton) findViewById(R.id.btnLoadImage);
        btnLoadImage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        // Add new advertisement
        if (v.getId() == R.id.btnAdd) {
            String url = getString(R.string.api_address) + "advertisement/create";

            MultipartRequest request = new MultipartRequest(
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Gson gson = new Gson();
                            AddAdvertisementResponse addResponse
                                    = gson.fromJson(response, AddAdvertisementResponse.class);

                            if (addResponse.isSuccess()) {
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                Toast.makeText(
                                        getApplicationContext(),
                                        addResponse.getMessage(),
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
            ) {
                @Override
                public Map<String, File> getFiles() {
                    Map<String, File> files = new HashMap<>();
                    files.put("image", new File(imgPath));
                    return files;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("name", etName.getText().toString());
                    params.put("description", etDescription.getText().toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("UserId", getIntent().getStringExtra("UserId"));
                    return headers;
                }
            };

            request.setTag(REQUESTS_TAG);
            queue.add(request);
        }

        if (v.getId() == R.id.btnLoadImage) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // When an Image is picked
        if (requestCode == RESULT_LOAD_IMG) {
            if (resultCode == RESULT_OK && null != data) {
                try {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);

                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imgPath = cursor.getString(columnIndex);
                    cursor.close();

                    ivImage.setImageBitmap(BitmapFactory.decodeFile(imgPath));
                } catch (Exception e) {
                    Toast.makeText(this, getString(R.string.text_error),
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.text_image_havent_picked),
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (queue != null) {
            queue.cancelAll(REQUESTS_TAG);
        }
    }

}
